<?php

namespace studiocreativateam;

class Tags
{
    public static function convert($tags)
    {
        setlocale(LC_CTYPE, 'pl_PL.utf8');
        if (is_array($tags)) $tags = implode(',', $tags);
        $tags = preg_replace('/^\[u\'/', '#', $tags);
        $tags = preg_replace('/\',\s*u\'/', ',#', $tags);
        $tags = preg_replace('/\'\]$/', '', $tags);
        $tags = preg_replace('/#/', '', $tags);
        $tags = preg_replace('/\s*,/', ',', $tags);
        $tags = preg_replace('/,\s*/', ',', $tags);
        $tags = Utils::convertFromUnicode($tags);
        $tags = iconv('UTF-8', 'ASCII//TRANSLIT', $tags);
        $tags = preg_replace("/'/", '', $tags);
        $tags = strtolower($tags);
        $tags = preg_replace('/[[:^print:]]/', '', $tags);
        return explode(',', trim($tags));
    }

    public static function parse($text, $converted = true)
    {
        preg_match_all('/(\#)([^\s]+)/', $text, $matches);
        $tags = $matches[2];
        if ($converted) $tags = static::convert($tags);
        return $tags;
    }
}
<?php

namespace studiocreativateam;

class Filesystem
{
    public static function tempdir($dir = '', $prefix = '', $mode = 0700)
    {
        if (!$dir) $dir = sys_get_temp_dir();
        if (substr($dir, -1) != '/') $dir .= '/';

        do {
            $path = $dir . $prefix . mt_rand(0, 9999999);
        } while (!mkdir($path, $mode));

        return $path;
    }

    public static function prepareFileName($path)
    {
        $parts = pathinfo($path);
        $filename = $parts['filename'];
        $filename = iconv("utf-8", "us-ascii//IGNORE//TRANSLIT", $filename);
        $replacements = [
            '/\s/' => '-',                // remove whitespace
            '/_/' => '-',                 // underscores to dashes
            '/[^A-Za-z0-9+.\-]+/' => '',  // remove non-ASCII chars, only allow alphanumeric plus dash and dot
            '/[\-]{2,}/' => '-',          // remove duplicate dashes
            '/^[\.\-_]+/' => '',          // Remove all leading dots, dashes or underscores
        ];
        foreach ($replacements as $regex => $replace) {
            $filename = preg_replace($regex, $replace, $filename);
        }
        $filename = preg_replace('/\s+/', '_', $filename);
        $filename = preg_replace('/_+/', '_', $filename);
        /*    $matches = null;
            if(preg_match('/-([\d]+)$/', $filename, $matches, PREG_OFFSET_CAPTURE))
            {
              $counter = (int)$matches[1][0];
              $filename = substr($filename, 0, $matches[0][1]);
            }
            else */
        $counter = 1;

        $dir = $parts['dirname'] . '/';
        $name = sprintf("%s.%s", $filename, $parts['extension']);
        while (is_file($dir . $name)) {
            $name = sprintf("%s-%d.%s", $filename, ++$counter, $parts['extension']);
        }
        return $dir . $name;
    }
}
<?php

namespace studiocreativateam;

class Utils
{
    public static $salt = 'Vm1wR2IyUXhUWGhUV0d4VlYwZDRWMWxVUW5kVmJGcHlWV3RLVUZWVU1Eaz0=';

    public static function createHash($len = 8, $id = '', $chars = '')
    {
        $hex = md5(static::$salt . uniqid($id, true));
        $pack = pack('H*', $hex);
        $uid = base64_encode($pack);
        $uid = static::filterHashChars($uid, $chars);
        $len = max(min($len, 128), 4);
        while (strlen($uid) < $len) {
            $uid = $uid . static::createHash(22);
        }
        return substr($uid, 0, $len);
    }

    public static function filterHashChars($str, $chars = '')
    {
        if (empty($chars)) $chars = 'A-Za-z0-9';
        return preg_replace("/[^" . $chars . "]/", "", $str);
    }

    public static function xor_string($value, $key)
    {
        $xored = "";
        for ($i = 0; $i < strlen($value); $i++) {
            $xored .= chr(ord($value{$i}) ^ ord($key{$i % strlen($key)}));
        }
        return $xored;
    }

    public static function micro_now()
    {
        return date("Y-m-d H:i:s") . substr((string)microtime(), 1, 7);
    }

    public static function micro_diff($micro1, $micro2 = null)
    {
        if (!$micro2) $micro2 = static::micro_now();
        $diff = strtotime($micro2) - strtotime($micro1);
        $diff += (double)preg_replace('/^[^\.]+/', '0', $micro2);
        $diff -= (double)preg_replace('/^[^\.]+/', '0', $micro1);
        return (int)($diff * 1000000);
    }

    public static function isCLI()
    {
        if (defined('STDIN')) {
            return true;
        }
        if (empty($_SERVER['REMOTE_ADDR']) and !isset($_SERVER['HTTP_USER_AGENT']) and count($_SERVER['argv']) > 0) {
            return true;
        }
        return false;
    }

    public static function liczebnik($text, $num)
    {
        $suffix = '';
        if ($num != 1) {
            $text = preg_replace('/ąd/', 'ęd', $text); // bo błąd -> błędów / błędy
            if ($num == 0
                || $num >= 5 && $num <= 21
                || $num > 20 && ($num % 10 == 0 || $num % 10 == 1 || $num % 10 >= 5)) {
                $suffix = 'ów';
            } else {
                $suffix = 'y';
            }
        }
        return $text . $suffix;
    }

    public static function singularOrPluralPL($num, $singular, $plural1, $plural2)
    {
        if ($num == 1) return $singular;
        else {
            if ($num == 0
                || $num >= 5 && $num <= 21
                || $num > 20 && ($num % 10 == 0 || $num % 10 == 1 || $num % 10 >= 5)) {
                return $plural2;
            } else {
                return $plural1;
            }
        }
    }

    public static function convertFromUnicode($text)
    {
        return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $text);
    }

    public static function removeEmoji($text)
    {
        $clean_text = "";

        // // Match Emoticons
        // $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        // $clean_text = preg_replace($regexEmoticons, '', $text);

        // // Match Miscellaneous Symbols and Pictographs
        // $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        // $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // // Match Transport And Map Symbols
        // $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        // $clean_text = preg_replace($regexTransport, '', $clean_text);

        // // Match Miscellaneous Symbols
        // $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        // $clean_text = preg_replace($regexMisc, '', $clean_text);

        // // Match Dingbats
        // $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        // $clean_text = preg_replace($regexDingbats, '', $clean_text);

        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace([
            // '/[\x{1F600}-\x{1F64F}]/u', // Match Emoticons
            // '/[\x{1F300}-\x{1F5FF}]/u', // Match Miscellaneous Symbols and Pictographs
            // '/[\x{1F680}-\x{1F6FF}]/u', // Match Transport And Map Symbols
            // '/[\x{2600}-\x{26FF}]/u',   // Match Miscellaneous Symbols
            // '/[\x{2700}-\x{27BF}]/u',   // Match Dingbats
            '/[\x{1F600}-\x{1F64F}]/u', // Match Emoticons
            '/[\x{1F300}-\x{1F5FF}]/u', // Match Miscellaneous Symbols and Pictographs
            '/[\x{1F680}-\x{1F6FF}]/u', // Match Transport And Map Symbols
            '/[\x{2600}-\x{26FF}]/u',   // Match Miscellaneous Symbols
            '/[\x{2700}-\x{27BF}]/u',   // Match Dingbats
        ], '', $text);

        return $clean_text;
    }

    public static function from_camel_case($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
<?php

namespace studiocreativateam;

class Email
{
    protected static $munged = [];

    public static function getCoded($email)
    {
        if (!isset(static::$munged[$email])) {
            $coded = "";
            $unmixedkey = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.@-";
            $inprogresskey = $unmixedkey;
            $mixedkey = "";
            $unshuffled = strlen($unmixedkey);
            for ($i = 0; $i < strlen($unmixedkey); $i++) {
                $ranpos = rand(0, $unshuffled - 1);
                $nextchar = $inprogresskey{$ranpos};
                $mixedkey .= $nextchar;
                $before = substr($inprogresskey, 0, $ranpos);
                $after = substr($inprogresskey, $ranpos + 1, $unshuffled - ($ranpos + 1));
                $inprogresskey = $before . '' . $after;
                $unshuffled -= 1;
            }
            $cipher = $mixedkey;
            $shift = strlen($email);
            for ($j = 0; $j < strlen($email); $j++) {
                if (strpos($cipher, $email{$j}) == -1) {
                    $chr = $email{$j};
                    $coded .= $email{$j};
                } else {
                    $chr = (strpos($cipher, $email{$j}) + $shift) % strlen($cipher);
                    $coded .= $cipher{$chr};
                }
            }
            static::$munged[$email] = [
                'coded' => $coded,
                'key' => $cipher
            ];
        }
        return static::$munged[$email];
    }

    public static function mungeHTML($html)
    {
        $pattern = '/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i';
        if (preg_match_all($pattern, $html, $matches)) {
            foreach (array_unique($matches[0]) as $email) {
                $coded = static::getCoded($email);
                $obfuscated = '{email:' . implode(':', array_values($coded)) . '}';
                $html = str_replace($email, $obfuscated, $html);
            }
            // $tag = '<script';
            // $pos = mb_strpos($html, $tag);
            // if($pos === FALSE)
            // {
            //   $tag = '</head>';
            //   $pos = mb_strpos($html, $tag);
            // }
            $tag = '</body>';
            $pos = mb_strpos($html, $tag);
            $html = mb_substr($html, 0, $pos)
                . <<<HTML
<style type="text/css">[href*="mailto:{email:"]{display: none;}</style><script type="text/javascript">document.body.innerHTML=document.body.innerHTML.replace(/\{email:([^:]+):([^\}]+)\}/g,function(){return function(e){for(var n=e.coded,t=e.key,r=n.length,o="",u=0;u<r;u++)if(-1==t.indexOf(n.charAt(u)))o+=n.charAt(u);else{var i=(t.indexOf(n.charAt(u))-r+t.length)%t.length;o+=t.charAt(i)}return o}({coded:arguments.length<=1?void 0:arguments[1],key:arguments.length<=2?void 0:arguments[2]})})</script>
HTML
                . $tag
                . mb_substr($html, $pos + mb_strlen($tag));
        }
        return $html;
    }
}
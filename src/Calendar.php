<?php

namespace studiocreativateam;

use Carbon\Carbon;

class Calendar
{
    protected static $constant = [
        '01-01' => [
            'id' => 'new-years-day',
            'title' => 'Nowy Rok',
        ],
        '01-06' => [
            'id' => 'epiphany',
            'title' => 'Trzech Króli',
        ],
        '05-01' => [
            'id' => 'may-day',
            'title' => "Święto Pracy",
        ],
        '05-03' => [
            'id' => 'constitution-day',
            'title' => "Święto Konstytucji 3 Maja",
        ],
        '08-15' => [
            'id' => 'assumption',
            'title' => "WNMP",
        ],
        '11-01' => [
            'id' => 'all-saints',
            'title' => "Wszystkich Świętych",
        ],
        '11-11' => [
            'id' => 'independence',
            'title' => "Dzień Niepodległości",
        ],
        '12-25' => [
            'id' => 'christmas1',
            'title' => "Boże Narodzenie",
        ],
        '12-26' => [
            'id' => 'christmas2',
            'title' => "Boże Narodzenie",
        ],
    ];

    public static function holidays($start = null, $end = null)
    {
        if (is_string($start)) $start = Carbon::parse($start);
        if (is_string($end)) $end = Carbon::parse($end);
        $start = empty($start) ? Carbon::now()->startOfYear() : $start->copy()->startOfDay();
        $end = empty($end) ? Carbon::now()->endOfYear() : $end->copy()->endOfDay();

        $result = [];
        $y = $start->year;
        do {
            $year = static::holidaysForYear($y);
            for ($i = 0; $i < count($year); $i++) {
                $date = Carbon::parse($year[$i]['date']);
                if ($date->diffInSeconds($start, false) > 0) continue;
                if ($date->diffInSeconds($end, false) <= 0) break;
                $result[] = $year[$i];
            }
            $y++;
        } while ($i == count($year));
        return $result;
    }

    public static function holidaysForYear($year)
    {
        $result = [];
        foreach (static::$constant as $key => $value) {
            $result[] = array_merge($value, ['date' => $year . '-' . $key]);
        }
        $easter = static::easter($year);
        $result[] = [
            'id' => 'easter1',
            'title' => 'Wielkanoc',
            'date' => $easter->format('Y-m-d')
        ];
        $result[] = [
            'id' => 'easter2',
            'title' => "Poniedziałek Wielkanocny",
            'date' => (clone $easter)->add(1, 'day')->format('Y-m-d')
        ];
        $result[] = [
            'id' => 'pentecost',
            'title' => "Zielone Świątki",
            'date' => (clone $easter)->add(49, 'day')->format('Y-m-d')
        ];
        $result[] = [
            'id' => 'corpus-christi',
            'title' => "Boże Ciało",
            'date' => (clone $easter)->add(60, 'day')->format('Y-m-d')
        ];
        usort($result, function ($a, $b) {
            return strcmp($a['date'], $b['date']);
        });
        return $result;
    }

    public static function easter($year)
    {
        $tmp = [
            [
                'years' => [33, 1582],
                'A' => 15,
                'B' => 6,
                'exceptions' => [[], []]
            ],
            [
                'years' => [1583, 1699],
                'A' => 22,
                'B' => 2,
                'exceptions' => [[1609], []]
            ],
            [
                'years' => [1700, 1799],
                'A' => 23,
                'B' => 3,
                'exceptions' => [[], []]
            ],
            [
                'years' => [1800, 1899],
                'A' => 23,
                'B' => 4,
                'exceptions' => [[], []]
            ],
            [
                'years' => [1900, 2099],
                'A' => 24,
                'B' => 5,
                'exceptions' => [[1981, 2076], [1954, 2049]]
            ],
            [
                'years' => [2100, 2199],
                'A' => 24,
                'B' => 6,
                'exceptions' => [[2133], [2106]]
            ],
            [
                'years' => [2200, 2299],
                'A' => 25,
                'B' => 0,
                'exceptions' => [[2201, 2296], []]
            ],
            [
                'years' => [2300, 2399],
                'A' => 26,
                'B' => 1,
                'exceptions' => [[], []]
            ],
            [
                'years' => [2400, 2499],
                'A' => 25,
                'B' => 1,
                'exceptions' => [[2448], []]
            ],
            [
                'years' => [2500, 2599],
                'A' => 26,
                'B' => 2,
                'exceptions' => [[], []]
            ],
            [
                'years' => [2600, 2699],
                'A' => 27,
                'B' => 3,
                'exceptions' => [[2668], []]
            ],
            [
                'years' => [2700, 2899],
                'A' => 27,
                'B' => 4,
                'exceptions' => [[2725, 2820], []]
            ],
            [
                'years' => [2900, 2999],
                'A' => 28,
                'B' => 5,
                'exceptions' => [[], []]
            ],
        ];

        for ($i = 0; $i < count($tmp); $i++) {
            if ($year >= $tmp[$i]['years'][0] && $year <= $tmp[$i]['years'][1]) {
                $data = $tmp[$i];
            }
        }
        if (empty($data)) return;
        extract($data);

        $a = $year % 19;
        $b = $year % 4;
        $c = $year % 7;
        $d = ($a * 19 + $A) % 30;
        $e = (2 * $b + 4 * $c + 6 * $d + $B) % 7;

        $easter = Carbon::parse($year . "-03-22");
        $easter->addDays($d + $e);
        if ($d == 29 && $e == 6) {
            $easter->subDays(7);
        } else
            if ($a > 10) {

            }
        return $easter;
    }
}